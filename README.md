# Swag labs test

## prerequisites

- java 8
- maven
- intellij
  - cucumber plugin
  - lombok plugin
  - google-java-format
- drivers - chrome drivers are for version 84 chrome

## running

mvn verify

## assumptions

- was lazy with currency parsing, given that
site switches between having dollar prefix
and not having it
- filtering would have been covered by
unit/integration tests
- the way ive written the feature file
could of been more reusable but often enough
people give into being developers and not
bridging business with BDD meaning you 
end up with Given And And And And Then And And And And etc...
- was unsure if I could use framework,
however used Serenity BDD