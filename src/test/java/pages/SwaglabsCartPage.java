package pages;

import models.Item;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.List;

@DefaultUrl("page:cart.page")
public class SwaglabsCartPage extends PageObject {

  @FindBy(partialLinkText = "CHECKOUT")
  private WebElementFacade checkoutButton;

  public void clickCheckout() {
    checkoutButton.click();
  }

  public List<Item> getBasketItems() {

    List<WebElementFacade> cartItems = findAll(By.className("cart_item"));

    List<Item> toReturn = new ArrayList<>();

    for (WebElementFacade webElementFacade : cartItems) {
      toReturn.add(
          new Item(
              webElementFacade.find(By.className("inventory_item_name")).getText(),
              Double.valueOf(webElementFacade.find(By.className("inventory_item_price")).getText()),
              webElementFacade.find(By.className("inventory_item_desc")).getText(),
              Integer.valueOf(webElementFacade.find(By.className("cart_quantity")).getText())));
    }

    return toReturn;
  }
}
