package pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("page:login.page")
public class SwaglabsLoginPage extends PageObject {

  @FindBy(id = "user-name")
  private WebElementFacade username;

  @FindBy(id = "password")
  private WebElementFacade password;

  @FindBy(id = "login-button")
  private WebElementFacade loginButton;

  public void login(String username, String password) {
    this.username.sendKeys(username);
    this.password.sendKeys(password);
    loginButton.click();
  }
}
