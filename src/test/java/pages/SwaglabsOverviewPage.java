package pages;

import models.Item;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.List;

@DefaultUrl("page:overview.page")
public class SwaglabsOverviewPage extends PageObject {

  @FindBy(partialLinkText = "FINISH")
  private WebElementFacade finishButton;

  @FindBy(className = "summary_subtotal_label")
  private WebElementFacade totalBeforeTax;

  @FindBy(className = "summary_tax_label")
  private WebElementFacade taxValue;

  @FindBy(className = "summary_total_label")
  private WebElementFacade totalWithTax;

  public void clickFinish() {
    finishButton.click();
  }

  public Double getTotalBeforeTax() {
    return Double.valueOf(totalBeforeTax.getText().replace("Item total: $", ""));
  }

  public Double getTax() {
    return Double.valueOf(taxValue.getText().replace("Tax: $", ""));
  }

  public Double getTotalWithTax() {
    return Double.valueOf(totalWithTax.getText().replace("Total: $", ""));
  }

  public List<Item> getBasketItems() {

    List<WebElementFacade> cartItems = findAll(By.className("cart_item"));

    List<Item> toReturn = new ArrayList<>();

    for (WebElementFacade webElementFacade : cartItems) {
      toReturn.add(
              new Item(
                      webElementFacade.find(By.className("inventory_item_name")).getText(),
                      Double.valueOf(webElementFacade.find(By.className("inventory_item_price")).getText().replace("$", "")),
                      webElementFacade.find(By.className("inventory_item_desc")).getText(),
                      Integer.valueOf(webElementFacade.find(By.className("summary_quantity")).getText())));
    }

    return toReturn;
  }
}
