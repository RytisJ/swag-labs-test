package pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("page:your.information.page")
public class SwaglabsYourInformationPage extends PageObject {

  @FindBy(id = "first-name")
  private WebElementFacade firstName;

  @FindBy(id = "last-name")
  private WebElementFacade lastName;

  @FindBy(id = "postal-code")
  private WebElementFacade postalCode;

  @FindBy(className = "cart_button")
  private WebElementFacade continueButton;

  public void fillOutOrderInformation(String firstName, String lastName, String postalCode) {
    this.firstName.sendKeys(firstName);
    this.lastName.sendKeys(lastName);
    this.postalCode.sendKeys(postalCode);
  }

  public void clickContinue() {
    continueButton.click();
  }
}
