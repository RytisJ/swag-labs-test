package pages;

import models.Item;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;

import java.util.List;

@DefaultUrl("page:cart.page")
public class SwaglabsProductPage extends PageObject {

  @FindBy(className = "product_sort_container")
  private WebElementFacade productFilter;

  @FindBy(id = "shopping_cart_container")
  private WebElementFacade checkoutIcon;

  public void filterByLowestPrice() {
    productFilter.selectByValue("lohi");
  }

  public void filterByHighestPrice() {
    productFilter.selectByValue("hilo");
  }

  public void clickCheckout() {
    checkoutIcon.click();
  }

  public List<WebElementFacade> getAllItemsOnPage() {
    return findAll(By.className("inventory_item"));
  }

  public void addToCart(WebElementFacade item) {
    item.find(By.className("pricebar")).find(By.cssSelector("button")).click();
  }

  public Item elementToItem(WebElementFacade webElementFacade) {
    return new Item(
        webElementFacade.find(By.className("inventory_item_name")).getText(),
        Double.valueOf(
            webElementFacade.find(By.className("inventory_item_price")).getText().replace("$", "")),
        webElementFacade.find(By.className("inventory_item_desc")).getText(),
        1);
  }
}
