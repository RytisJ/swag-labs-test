package models;

import lombok.Data;

@Data
public class Item {
    private final String itemName;
    private final Double itemPrice;
    private final String itemDescription;
    private final Integer quantity;
}
