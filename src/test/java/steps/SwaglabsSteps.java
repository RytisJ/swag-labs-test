package steps;

import com.github.javafaker.Faker;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import models.Item;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.pages.WebElementFacade;
import pages.*;

import java.text.NumberFormat;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static utils.SharedStateConstants.BASKET;

public class SwaglabsSteps {

  private final Faker faker = new Faker();
  private SwaglabsLoginPage loginPage;
  private SwaglabsProductPage productPage;
  private SwaglabsCartPage cartPage;
  private SwaglabsYourInformationPage yourInformationPage;
  private SwaglabsOverviewPage overviewPage;
  private SwaglabsOrderConfirmationPage confirmationPage;

  @Given("I am logged into swaglabs")
  public void iAmLoggedIntoSwaglabs() {
    loginPage.open();
    loginPage.login("standard_user", "secret_sauce");
  }

  @And("I filter for cheapest item and most expensive item and add it to cart")
  public void iFilterForCheapestItemAndMostExpensiveItemAndAddItToCart() throws Exception {
    productPage.filterByLowestPrice();

    List<WebElementFacade> allItemsOnPageFilteredByLowestPrice = productPage.getAllItemsOnPage();
    assertThat(allItemsOnPageFilteredByLowestPrice.size()).isNotEqualTo(0);

    WebElementFacade cheapestItem = allItemsOnPageFilteredByLowestPrice.get(0);
    productPage.addToCart(cheapestItem);

    productPage.filterByHighestPrice();
    List<WebElementFacade> allItemsOnPageFilteredByHighestPrice = productPage.getAllItemsOnPage();
    assertThat(allItemsOnPageFilteredByHighestPrice.size()).isNotEqualTo(0);

    WebElementFacade mostExpensiveItem = allItemsOnPageFilteredByHighestPrice.get(0);
    productPage.addToCart(mostExpensiveItem);

    Serenity.setSessionVariable(BASKET)
        .to(
            Arrays.asList(
                productPage.elementToItem(cheapestItem),
                productPage.elementToItem(mostExpensiveItem)));
  }

  @When("I check my basket and enter my details")
  public void iCheckMyBasketAndEnterMyDetails() {
    productPage.clickCheckout();

    assertThat(cartPage.getBasketItems())
        .containsExactlyElementsOf(Serenity.sessionVariableCalled(BASKET));

    cartPage.clickCheckout();

    // didn't appear anywhere therefore no need to save it
    yourInformationPage.fillOutOrderInformation(
        faker.name().firstName(), faker.name().lastName(), faker.address().zipCode());

    yourInformationPage.clickContinue();
  }

  @Then("purchase is complete")
  public void purchaseIsComplete() {
    List<Item> basket = Serenity.sessionVariableCalled(BASKET);

    Double beforeTaxValue = overviewPage.getTotalBeforeTax();

    assertThat(overviewPage.getBasketItems()).containsExactlyElementsOf(basket);

    assertThat(beforeTaxValue).isEqualTo(basket.stream().mapToDouble(Item::getItemPrice).sum());

    NumberFormat nf = NumberFormat.getNumberInstance();
    nf.setGroupingUsed(false);
    nf.setMinimumFractionDigits(2);

    assertThat(overviewPage.getTotalWithTax())
        .isEqualTo(Double.valueOf(nf.format(beforeTaxValue + overviewPage.getTax())));

    overviewPage.clickFinish();
    assertThat(confirmationPage.getConfirmationHeader()).isEqualTo("THANK YOU FOR YOUR ORDER");
  }
}
