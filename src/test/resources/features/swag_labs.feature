@swaglabs
Feature: testing shopping journeys

  Scenario: filtering for most expensive and cheapest product with full checkout
    Given I am logged into swaglabs
    And I filter for cheapest item and most expensive item and add it to cart
    When I check my basket and enter my details
    Then purchase is complete